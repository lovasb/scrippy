import ast

from scrippy.api import api
from scrippy.output import JavascriptModule


with open('example.py') as f:
    src = f.read()

tree = ast.parse(src, mode='exec')
print('Original tree: ')
print(ast.dump(tree, indent=4))
print('----------------------------------------------------------------')

new_tree = api.transform_ast(tree)

print('New tree: ')
print(ast.dump(new_tree, indent=4))
print('----------------------------------------------------------------')

js = JavascriptModule(new_tree)
js_text = js.read()
with open('example.js', 'w', encoding='utf-8') as f:
    print(js_text, file=f)
    print('//# sourceMappingURL=example.js.map', file=f)

print('Generated javascript')
print(js_text)
print('----------------------------------------------------------------')
smap = js.sourcemap(source=src, src_filename='example.py')
with open('example.js.map', 'w', encoding='utf-8') as f:
    print(smap.stringify(), file=f)
