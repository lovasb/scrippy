from django.urls import path

from . import views

urlpatterns = [
    path('index/', views.index, name='index'),
    path('vfs/poll/', views.poll_filesystem_changes, name='vfs-poll'),
]

app_name = 'backend'
