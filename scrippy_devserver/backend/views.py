import datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.core.cache import cache


def index(request):
    return render(request, 'index.html')


def tests(request):
    return render(request, 'tests.html')


def poll_filesystem_changes(request):
    return HttpResponse(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
