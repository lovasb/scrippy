from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('backend/', include('scrippy_devserver.backend.urls', namespace='backend')),
]
