$S.int = class int extends $S.PyMetaClass() {
    constructor(value) {
        super();
        this.__init__(value)
    }
    __init__(value) {
        this._value = Number.parseInt(value);
        if (isNaN(this._value)) {
            throw $S.PyCall(TypeError, "'" + typeof value + "' object cannot be interpreted as an integer");
        }
    }

    __eq__(other) {
        if ($S.isinstance(other, int)) {
            return this._value === other._value;
        } else if (typeof other === 'number') {
            return this._value === Number.parseInt(other);
        }
    }
}

class Integer {
    __new__ = () => {
        console.log(this)
    }
}

Integer.__new__ = () => {
    console.log('*************', this)
}

console.log(Integer.__new__())