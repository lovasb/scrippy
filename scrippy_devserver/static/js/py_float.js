$S.float = class float extends $S.PyMetaClass() {
    constructor(value) {
        super();
        this.__init__(value)
    }
    __init__(value) {
        this._value = Number.parseFloat(value);
        if (isNaN(this._value)) {
            throw $S.PyCall(ValueError, ["could not convert string to float: '" + value + "'"]);
        }
    }

    __eq__(other) {
        if ($S.isinstance(other, float)) {
            return this._value === other._value;
        } else if (typeof other === 'number') {
            return this._value === Number.parseFloat(other);
        }
    }
}
