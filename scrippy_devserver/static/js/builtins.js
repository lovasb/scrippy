'use strict';

var SCRIPPY = {}
const $S = SCRIPPY || {}

/*SCRIPPY.bound_method = class bound_method extends Function {
  constructor(obj, method) {
    super('...args', 'return this._bound._call(...args)')
    // Or without the spread/rest operator:
    // super('return this._bound._call.apply(this._bound, arguments)')
    this._bound = this.bind(this)
    this._bound.obj = obj;
    this._bound.method = method;

    return this._bound
  }

  _call(...args) {
      return this.method(this.obj, ...args);
  }
}*/

/* SCRIPPY.bound_method = class Callable extends Function {
  constructor(obj, method) {
    super('return arguments.callee._call.apply(arguments.callee, arguments)')
    // We can't use the rest operator because of the strict mode rules.
    // But we can use the spread operator instead of apply:
    // super('return arguments.callee._call(...arguments)')
      this.obj = obj;
      this.method = method;
  }

  _call(...args) {
    return this.method(this.obj, ...args);
  }
}
SCRIPPY.bound_method = (...args) => { return attr(target, ...args) };*/
SCRIPPY.bound_method = function (obj, method) {
    return (...args) => { return  method(obj, ...args); }
}

/*
    Python <class 'type'>
 */
SCRIPPY.Type = class Type extends Function {
    constructor(name_, bases, klass) { /* metaclass.new */
        super();

        this.name_ = name_;
        this.bases = bases;
        this.klass_def = klass;
        this.klass_def.__class__ = Type;

        this._cls = new Proxy(this, {
            apply: (target, _, args) => target.__new__(...args),
            get: (target, p) => {
                if (this.hasOwnProperty(p)) { return this[p]; }
                // if (p in ['name_', 'klass_def', 'bases']) { return this[p] }
                let attr = this.klass_def[p];
                // console.log(p, attr)
                if (attr === undefined) {
                    for (let base of this.bases) {
                        if (base[p] !== undefined) {
                            attr = base[p]
                            break
                        }
                    }
                }

                if (attr === undefined) {
                    throw Error
                }
                // console.assert(attr !== undefined)  // TODO: Attributeerror
                return attr;
            },
            /*set: (target, p, value) => {
            }*/
        });

        return this._cls;
    }

    __new__ = (...args) => { /* class.new */
        let cls = this._cls;
        let obj = new this.klass_def();
        obj.__class__ = cls;

        let obj_proxy = new Proxy(obj, {
            get(target, p) {
                // console.log("object proxy", p, target)
                let attr = target[p] || cls[p];

                if (typeof attr === 'function') {  /* function call, but the function is not the class proxy itself */
                    if (attr === cls) { /* obj.__class__ is a function */
                        return cls
                    } else {
                        let bm = new $S.bound_method(target, attr);
                        bm.obj = target;
                        bm.method = attr;
                        return bm;
                    }

                } else {
                    attr = target['__getattr__'] || cls['__getattr__']
                    return attr.call(target, p);
                }
            },
            set(target, p, value) {
                let sattr = target['__setattr__'] || cls['__setattr__'];

                if (sattr !== undefined) {
                    sattr.call(target, p, value);
                } else {
                    target[p] = value;
                }

                return true;
            }
        });

        obj_proxy.__init__(...args);
        return obj_proxy;
    }
}

console.print = ($args, $kwargs) => {
    let new_args = []
    for (let arg of $args) {
        if (arg !== undefined && arg.__class__ === SCRIPPY.Type) {  // it's a class
            new_args.push("<class " + arg.name_ + ">")
        } else if (arg !== undefined && arg.__repr__ !== undefined) {  // has __repr__ method
            new_args.push(arg.__repr__())
        } else if (typeof arg === 'function') {  // function call
            if (arg.obj !== undefined && arg.method !== undefined) {
                new_args.push('<bound method ' + arg.obj.__class__.name_ + "." + arg.method.name + '>')
            } else {
                new_args.push('<function ' + arg.name + '>')  /* TODO: class method: <function Foo.valami at 0x7f5a0eb01af0> */
            }
        } else {
            new_args.push(arg)
        }
    }

    console.log(...new_args);
}

/*
    Python type(obj)
 */
SCRIPPY.type = (obj) => {
    return obj['__class__']
}

/*
    Python object
 */
SCRIPPY.object = new $S.Type('object', [], class {
    static __init__ = (self, ...args) => {

    }

    static __setattr__ = (self, name, value) => {
        self[name] = value;
    }

    static __getattr__ = (self, key) => {
        return self[key];
    }

    static __repr__ = (self) => {
        return '<' + self.__class__.name_ + ' object>'
    }
});

SCRIPPY.assert = (test) => {
    if ([false, undefined, null].indexOf(test) !== -1) {
        throw Error('AssertionError');
    }

    return true;
}


/*
    act_args -- Actual arguments
    act_kwargs -- Actual keyword arguments
    func_def -- Argument definitions
 */
SCRIPPY.func_argparser = (act_args, act_kwargs, func_def) => {
    let retval = [];
    for (let i = 0; i < func_def.args.length; i++) {
        retval.push(act_args[i]);
    }

    if (func_def['vararg'] !== undefined) {
        retval.push(act_args.slice(func_def.args.length));
    } else {
        if (act_args.length > func_def.args.length) {
            throw Error("TypeError: function() takes " + func_def.args.length + " positional arguments but " + act_args.length + " were given");
        }
    }

    let kwargs_start = retval.length

    for (let i = 0; i < func_def.kwonlyargs.length; i++) { retval.push(undefined); }

    if (func_def['kwarg'] !== undefined) {
        retval.push({});
    }

    for (let [k, v] of Object.entries(act_kwargs)) {
        /* positional argument as keyword arg */
        let i = func_def.args.indexOf(k);
        if (i !== -1) {
            if (retval[i] !== undefined) {
                throw Error("TypeError: function() got multiple values for argument '" + k + "'"); /* TODO: throw TypeError */
            }
            retval[i] = v;
            continue;
        }

        /* named keyword arguments */
        i = func_def.kwonlyargs.indexOf(k);
        if (i !== -1) {
            retval[kwargs_start + func_def.kwonlyargs.indexOf(k)] = v;
            continue;
        }

        /* kwargs rest */
        console.assert(func_def.kwarg !== undefined);
        retval[retval.length - 1][k] = v;
    }

    for (let i = 0; i < func_def.kw_defaults.length; i++) {
        if (retval[kwargs_start + i] === undefined) {
            retval[kwargs_start + i] = func_def.kw_defaults[i];
        }
    }

    let idx = retval.indexOf(undefined);
    if (idx !== -1) {
        throw Error("TypeError: function() missing 1 required positional argument: '" + func_def.args[idx] + "')"); /* TODO: throw TypeError */
    }

    return retval
}

/*
    Python-like calling.
        Calling classes: Klass()
        Calling function: func()
        Calling callable object with __call__
 */
SCRIPPY.PyCall = (callable, ...args) => {
    if (SCRIPPY.type(callable) === SCRIPPY.Type) {
        obj = new callable(...args)
        obj.__class__ = callable
        return obj
    } else if (callable['__call__'] !== undefined) {
        return callable.__call__(...args)
    } else {
        return callable(...args)
    }
}

SCRIPPY.helpers = {}
SCRIPPY.helpers.zip_longest = (xs, ys) => {
  const zipped = []
  for (let i = 0; i < Math.max(xs.length, ys.length); i++) {
    zipped.push([xs[i], ys[i]])
  }
  return zipped
}

SCRIPPY.PyArguments = (act_args, act_kwargs, settings) => {
    args = act_args.slice(0, settings['args'].length)  // SCRIPPY.helpers.zip_longest(act_args.slice(0, settings['args'].length), settings['args'])

    if (settings['vararg'] !== undefined) {
        args.push(act_args.slice(settings['args'].length, act_args.length))
    }

    return args
}

SCRIPPY.hasattr = (__obj, __name) => {
    return __obj[__name] !== undefined;
}

SCRIPPY.eq = (left, right) => {
    if ($S.hasattr(left, '__eq__')) {
        return left.__eq__(right);
    } else if ($S.hasattr(right, '__eq__')) {
        return right.__eq__(left);
    } else {
        return left === right;
    }
}

SCRIPPY.isinstance = (__obj, __class_or_tuple) => {
    if (__class_or_tuple.__class__ === $S.Type) {
        return __obj.__class__ === __class_or_tuple;
    } else {
        // tuple, not implemented
    }

    return false;
}