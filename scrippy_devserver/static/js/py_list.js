
class list extends $S.PyMetaClass() {
    constructor() {
        super();
        this._array = [1, 2, 3, 4]
    }

    __getitem__ = function(key) {
        // var $ = $B.args("__getitem__",2,{self: null, key: null},
        //     ["self", "key"], arguments, {}, null, null),
        //     self = $.self,
        //     key = $.key
        //$B.check_no_kw("__getitem__", self, key)
        //$B.check_nb_args("__getitem__", 2, arguments)
        return this._array[key]
    }
}

l = new list();


l_p = new Proxy(l, {
    get(target, name) {
        if ('__getitem__' in target) {
            return target.__getitem__(name)
        }
    },

    set(target, name, value) {
        console.log(name, value)
        target._array[name] = value
        // console.log('setter', target, name, value)
    }
});

// console.log(l_p.__getitem__([5,6]))
// console.log(l_p(5)) // py_string.js:31 Uncaught TypeError: l_p is not a function
// console.log(l_p.valami)

// l.__getitem__(6)

///let v = 7;
/*console.log(l_p[0])

for (part of [l_p[0], v]) {
    part = 2
}
// [l_p[0], v] = [7, 8]
// l_p[1] = 8
console.log(l_p[0])*/

///let a = ['first', 'second'];

/*let v = 7;
for (part of [l_p[0], v]) {
    console.log(part)
    part = 2
}*/

let arr = new Array()
console.log(arr[Symbol.iterator])
l_p["2:7"] = [7, 8]

function* iterable() {
    yield 1;
    yield 2;
    yield 3;
}

for (let x of iterable()) {
    console.log(x);
}

let y = iterable()
console.log(y[Symbol.iterator])
console.log(y.next())
console.log(y.next())
console.log(y.next())
console.log(y.next())
/*function func() {
    return [7, 8, 9]
}

[l_p[0], v] = func()
console.log(l_p[0], v)*/

/*
    a = list([7,8,9])
    a.append('valami')
    a[0]
    a[0], b = 2,3

    class A:
        def __getitem__(self):
            pass

    a = A()
    a[5]
 */