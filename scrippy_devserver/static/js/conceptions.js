function func($args, $kwargs) {
    let [a, b, c, kwargs] = $S.func_argparser($args, $kwargs, {"args": ["a", "b", "c"], "kwarg": "c", "kwonlyargs": [], "kw_defaults": [], "defaults": []});
    return [a, b, c, kwargs];
}
let retval = func([1, 2],{})
$S.assert((retval === [1, 2, 7, {}]));
