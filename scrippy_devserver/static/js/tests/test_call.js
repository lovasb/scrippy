
/*
    Calling classes without new operator
 */
class Example extends $S.PyMetaClass() {

}

obj = $S.PyCall(Example)
console.assert($S.type(obj) == Example)


/*
    Calling function
 */
function example2(a) {
    return a * 2
}

result = $S.PyCall(example2, 4)
console.assert(result == 8)


/*
    Calling callable
 */

class Example3 extends $S.PyMetaClass() {
    __call__() {
        return 2
    }
}

obj = $S.PyCall(Example3)
result = $S.PyCall(obj)
console.assert(result == 2)
