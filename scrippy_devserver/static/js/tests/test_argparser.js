
/*
    Python:
     def function(arg1, arg2):
        pass

     function(1, 2)
*/
arg1 = null; arg2 = null;
[arg1, arg2] = $S.PyArguments(
    [1, 2],
    {},
    {'args': ['arg1', 'arg2']}
)
console.assert(arg1 === 1)
console.assert(arg2 === 2)
delete arg1; delete arg2;

/*
    Python:
     def function(arg1, arg2, *args):
        pass

     function(1, 2, 3, 4, 5)
*/
arg1 = null; arg2 = null; args = null;
[arg1, arg2, args] = $S.PyArguments(
    [1, 2, 3, 4, 5],
    {},
    {'args': ['arg1', 'arg2'], 'vararg': 'args'}
)
console.assert(arg1 === 1)
console.assert(arg2 === 2)
// console.assert()
// console.assert(args[0] === [3])


class List {
    constructor(...args) {
        this._arr = args
    }
}

l = new List()
myObjectProxy = new Proxy(l, {
   get: function (func, name) {
       console.log(func, name);
       if (name in l) {
           return l[name]
       }
       /*if( name in ListBase ) {
           return myObject[name];
       }
       // if it doesn't exists handle non-existing name however you choose
       return function (args) {
           console.log(name, args);
       }*/
    }
});

console.log(myObjectProxy instanceof List)
console.log(myObjectProxy[2])
// console.log(myObjectProxy.valami)
/*a = new myObjectProxy(1, 2, 3)
console.log(a instanceof List)
console.log(a[2])*/
