
a = $S.PyCall($S.int, 7);
b = $S.PyCall($S.int, "7");
console.assert($S.isinstance(a, $S.int))
console.assert($S.eq(a, b));
console.assert($S.eq(a, 7));
