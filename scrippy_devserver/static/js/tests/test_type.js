
class A extends $S.PyMetaClass() {

}

/*
class A:
    pass

A.__class__ == type
type(A) == type
 */
console.assert(A.__class__ == $S.Type)
console.assert($S.type(A) == $S.Type)


class B extends $S.PyMetaClass(A) {

}

console.assert(B.__class__ == $S.Type)
console.assert($S.type(B) == $S.Type)


/*make_python_class(B)
let b = new B()
console.assert(new B().__class__ == B)*/
