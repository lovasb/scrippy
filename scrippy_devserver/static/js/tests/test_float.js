
a = $S.PyCall($S.float, 7.5);
b = $S.PyCall($S.float, "7.5");
console.assert($S.isinstance(a, $S.float))
console.assert($S.eq(a, b));
console.assert($S.eq(a, 7.5));

c = $S.PyCall($S.float, 7);
console.assert($S.eq(c, 7.0))

try {
    d = $S.PyCall($S.float, "valami")
} catch(err) {
    console.assert($S.isinstance(err, ValueError))
}