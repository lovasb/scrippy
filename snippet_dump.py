import ast

snippet = """
class A:
    def valami(self):
        pass

a = A()
a.valami()

def mas():
    pass
    
mas()
"""

snippet = """
a = a + 1 + 4
a == 7
"""

# a[2]   Expr(value=Subscript(value=Name(id='a', ctx=Load()), slice=Constant(value=2), ctx=Load())),
# a.append   Call

a = []
#getattr(a, 'append')(8)


tree = ast.parse(snippet)
print(ast.dump(tree))


# print(type(Exception))



"""
class Valami:
    ertek = 7

    def value(self, param, *args, param2=7, **kwargs):
        pass


class Mas:
    pass


v = v2 = v3 = Valami()
v.value(7, 8, 9, valami=8)
m = Mas()

print(v)
"""

"""
class A():
    def __getattribute__(self, item):
        print('getattribute', item)

    def __getattr__(self, item):
        print('getattr', item)

    def __getitem__(self, item):
        print(item.__class__)
        return "valami"

    def __eq__(self, other):
        return "a" == other

a = A()
print(a[2, 3])
print(list([2,3]))
# print(a.valami)

b = "a"
#print(b.__eq__("df"))
#print(b == a)
#print(a == b)
"""


"""class O:
    def __setitem__(self, key, value):
        print(key, value)

o = O()

o[4:6] = 4
print(o)

o2 = [6, 7, 8]
#o2[1::1] = [1, 2]
#print(o2)
o2[::-1] = o2
print(o2)"""


from py_mini_racer import py_mini_racer
ctx = py_mini_racer.MiniRacer()
retval = ctx.eval('var a = 8; 1+1; a = a + 1;')
print(retval)
retval = ctx.eval('a + 1')
print(retval)