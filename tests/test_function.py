from tests.base import ScrippyTestCase, PyScript


class FunctionTest(ScrippyTestCase):
    def test_function_definition(self):
        script = PyScript("""
        def func(a, b, c=7, **kwargs):
            return [a, b, c, kwargs]
            
        retval = func(1, 2)
        retval
        # assert retval == [1, 2, 7, {}]
        """)

        self.pyeval(script)

        js = self.py2js(script)
        print(js)
        retval = self.jseval(script=js)
        print(retval)
