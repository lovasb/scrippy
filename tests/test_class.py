from tests.base import ScrippyTestCase, PyScript


class ClassTest(ScrippyTestCase):
    def test_class_definition(self):
        script = PyScript("""
        class A(object):
            def __init__(self, num):
                num2 = num
                
        a = A(7)
        assert isinstance(a, A)
        #assert isinstance(A, type)
        """)

        self.pyeval(script)
        self.jseval(script)
        print(self.py2js(script))
