import datetime

from py_mini_racer import py_mini_racer
from unittest import TestCase
from scrippy.api import api


class PyScript:
    def __init__(self, content):
        self.content = ""

        assert len(content.split('\n')[0].strip()) == 0
        strip = content.split('\n', 2)[1]
        strip = len(strip) - len(strip.lstrip(' '))
        for line in content.split('\n'):
            line = line[strip:]
            if len(line) > 0:
                self.content += line + '\n'

    def __str__(self):
        return self.content


class ScrippyTestCase(TestCase):
    def setUp(self) -> None:
        self.v8 = py_mini_racer.MiniRacer()
        with open('scrippy_devserver/static/js/builtins.js') as f:
            self.v8.eval(f.read())

    def pyeval(self, script, glob=None):
        glob = glob or {}

        start = datetime.datetime.now()
        exec(str(script), glob)
        end = datetime.datetime.now()

        runtime = (end - start).total_seconds() * 1000  # runtime in milliseconds
        del glob['__builtins__']

        return runtime, glob

    def py2js(self, script):
        return api.transpile_string(str(script))

    def jseval(self, script):
        return self.v8.eval(script)
