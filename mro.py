class A:
    def who_am_i(self):
        print("I am a A")

class B(A):
    #def who_am_i(self):
    #    print("I am a B")
    pass

class Klass1:
    a = 7
    b = {}

class C(A):
    def who_am_i(self):
        print("I am a C")

class Meta(type):
    def __init__(self, *args, **kwargs):
        print('init')

    def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, bases, dct)
        x.attr = 100
        return x


class Foo(Klass1, metaclass=Meta):
    pass

class D(B, C, Foo):
    #def who_am_i(self):
    #    print("I am a D")
    pass

d1 = D()
d1.who_am_i()
print(D.__mro__)
