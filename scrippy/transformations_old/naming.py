from javascripthon.js_ast import JSTrue, JSFalse, JSNull, JSName
from javascripthon.transformations import _normalize_name


def Name_default(t, x):
    # {True,False,None} are Names
    cls = {
        'True': JSTrue,
        'False': JSFalse,
        'None': JSNull,
    }.get(x.id)
    if cls:
        return cls()
    else:
        n = x.id
        n = _normalize_name(n)
        return JSName(n)


Name = [Name_default]
