import re

from javascripthon.js_ast import JSExpressionStatement
from scrippy.js_ast.expressions import JSAttribute


def Expr_default(t, x):
    return JSExpressionStatement(x.value)


Expr = [Expr_default]  # Expr_docstring


def Attribute_default(t, x):
    return JSAttribute(x)


# Attribute = [Attribute_default]
