import ast

from javascripthon.js_ast import JSDict, JSList
from javascripthon.processor.transforming import Transformer


# FunctionDef(name='value', args=arguments(posonlyargs=[], args=[arg(arg='self')], kwonlyargs=[], kw_defaults=[], defaults=[]), body=[Pass()], decorator_list=[])
from scrippy.js_ast.functions import JSFunction


class FunctionTransformer:
    def func_type(self, t: Transformer, x: ast.FunctionDef):
        if isinstance(t.parent_of(x), ast.ClassDef):  # it is a class method
            return True

        return False

    def __call__(self, t: Transformer, x: ast.FunctionDef):
        func = JSFunction(x.body, args=[])
        func.name = x.name
        func.is_method = self.func_type(t, x)
        # print(ast.dump(x))

        func.python_args = x.args

        #print(func.args.args[0].arg)

        return func


FunctionDef = [
    FunctionTransformer()
]
