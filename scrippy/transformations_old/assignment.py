import ast

from scrippy.js_ast.expressions import JSAssignmentExpression
from javascripthon.processor.transforming import Transformer


def Assign_default(t: Transformer, x):
    is_global = True
    parent = t.parent_of(x)
    while isinstance(parent, ast.Module):
        if isinstance(parent, (ast.ClassDef, ast.FunctionDef)):
            is_global = False
            break

        parent = t.parent_of(parent)

    y = JSAssignmentExpression(x.targets[-1], x.value)
    for i in range(len(x.targets) - 1):
        y = JSAssignmentExpression(x.targets[-(2 + i)], y)

    #if is_global:
    #    y.options['export'] = True

    y.options['declaration'] = 'let'
    return y


Assign = [Assign_default]
