import ast

from ._constants import FUNC_JSFUNC_MAPPER
from javascripthon.js_ast import JSName, JSAttribute
from scrippy.js_ast.expressions import JSCall


def Call_default(t, x, operator=None):
    # See [pj.transformations.special](special.py) for special cases
    """kwkeys = []
    kwvalues = []
    if x.keywords:
        for kw in x.keywords:
            t.unsupported(x, kw.arg is None, "'**kwargs' syntax isn't "
                          "supported")
            kwkeys.append(kw.arg)
            kwvalues.append(kw.value)
        kwargs = JSDict(_normalize_dict_keys(t, kwkeys), kwvalues)
    else:
        kwargs = None"""

    if x.func.id in FUNC_JSFUNC_MAPPER.keys():
        x.func.id = FUNC_JSFUNC_MAPPER[x.func.id]

    return JSCall(x.func, x.args, {})


def Call_print(t, x):
    if (isinstance(x.func, ast.Name) and x.func.id == 'print'):
        return JSCall(JSAttribute(JSName('console'), 'print'), x.args)


Call = [Call_print, Call_default]
