import ast

from scrippy.js_ast.classes import JSClass
from javascripthon.js_ast import JSName, JSStatements


def ClassDef_default(t, x):
    name = x.name
    body = x.body

    if len(x.bases) > 0:
        superclass = x.bases
    else:
        superclass = ['object']

    # strip docs from body
    fn_body = [e for e in body if isinstance(e, (ast.FunctionDef,
                                                 ast.AsyncFunctionDef))]

    # assign incoming pynode to the JSClass for the sourcemap
    cls = JSClass(JSName(name), superclass, fn_body)
    cls.py_node = x

    stmts = [cls]

    return JSStatements(*stmts)


ClassDef = [ClassDef_default]
