from javascripthon.transformations.obvious import NameConstant, Num, Str
from scrippy.js_ast.literals import JSBool, JSNull, JSDataType


# Take care of Python 3.8's deprecations:
# https://docs.python.org/3/library/ast.html#node-classes
def Constant(t, x):
    if isinstance(x.value, bool):
        return JSBool(x.n)
    elif x.value is None:
        return JSNull()
    elif isinstance(x.value, (int, float, str)):
        return JSDataType(x.value.__class__.__name__, x.n)
    else:
        # Should constant collections (tuple and frozensets
        # containing constant elements) be handled here as well?
        # See https://greentreesnakes.readthedocs.io/en/latest/nodes.html#Constant
        raise ValueError('Unknown data type received.', x.value.__class__.__name__)
