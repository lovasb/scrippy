import ast

from .naming import JSConstant
from .base import JSNode


class JSExpr(JSNode, ast.Expr):
    def emit(self, generator: 'SourceGenerator'):
        retval = list(self.value.emit(generator))
        if isinstance(self.value, (JSConstant,)):
            retval = ['/*'] + retval + ['*/']

        yield self.line(retval, delim=True)
