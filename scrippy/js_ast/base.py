import ast
import inspect

from scrippy.output import Line, Part


class TargetNode:
    """This is the common ancestor of all the JS AST nodes."""

    """The associated Python AST node"""
    py_node = None

    def __str__(self):
        return ''.join(str(x) for x in self.serialize())

    def _chain(self, items):
        for i in self._expand(items):
            if inspect.isgenerator(i):
                yield from i
            else:
                yield i

    def _expand(self, items):
        for i in items:
            if isinstance(i, TargetNode):
                yield from i.serialize()
            else:
                yield i

    def emit(self, generator: 'SourceGenerator'):
        """This is the main output definition method. Is reimplemented by the
        subclasses."""
        raise NotImplementedError

    def line(self, item, indent=False, delim=False, name=None):
        if isinstance(item, Line):
            item.indent += int(indent)
            l = item
        elif isinstance(item, (tuple, list)):
            item = tuple(self._chain(item))
            l = Line(self, item, indent, delim, name)
        else:
            l = Line(self, item, indent, delim, name)
        return l

    def lines(self, items, *, indent=False, delim=False, name=None):
        if not isinstance(items, (tuple, list)):
            items = (items,)
        for i in self._chain(items):
            yield self.line(i, indent=indent, delim=delim, name=name)

    def part(self, *items, name=None):
        it = tuple(self._expand(items))
        if len(it) == 1 and isinstance(it[0], Line):
            result = it[0].item
        else:
            result = Part(self, *it, name=name)
        return result

    def serialize(self, generator: 'SourceGenerator' = None):
        for a in self.emit(generator):
            if hasattr(a, 'serialize'):
                yield from a.serialize(generator)
            else:
                yield a


class JSNode(TargetNode, ast.AST):
    def __init__(self, py_node):
        self.py_node = py_node

    def __getattr__(self, item):
        try:
            return object.__getattribute__(self, item)
        except AttributeError:
            return getattr(self.py_node, item)
