from javascripthon.js_ast.base import JSNode
from javascripthon.processor.util import delimited_multi_line


class JSLiteral(JSNode):
    def emit(self, text):
        yield from self.lines(delimited_multi_line(self, text, '', '', False))


class JSBool(JSLiteral):
    def emit(self, x):
        if x is True:
            yield self.part('true')
        elif x is False:
            yield self.part('false')
        else:
            raise NotImplementedError


class JSNull(JSLiteral):
    def emit(self):
        yield self.part('null')


class JSDataType(JSNode):
    def emit(self, data_type, value):
        if isinstance(value, str):
            value = f'"{value}"'
        yield self.part(f'{value}')
