import json
import re

from javascripthon.js_ast.base import JSNode, JSStatement
from javascripthon.processor.util import delimited


class JSCall(JSNode):

    operator = ''

    def emit(self, func, args, kwargs={}, operator=None):
        operator = operator or self.operator
        arr = [operator, func, '(', '[']
        fargs = args.copy()
        """if kwargs:
            fargs.append(kwargs)"""
        delimited(', ', fargs, dest=arr)
        arr += [']', ',']
        arr.append(json.dumps(kwargs))
        arr.append(')')
        yield self.part(*arr)


class JSAssignmentExpression(JSNode):
    def emit(self, left, right, export=False, declaration=None):
        line = [left, ' = ', right]

        if declaration:
            line.insert(0, 'let ')

        if export:
            line.insert(0, 'export ')

        yield self.line(line)


class JSThis(JSNode):
    """
    self. --> this.
    """
    def emit(self):
        yield self.part('this')


class JSAttribute(JSNode):
    def emit(self, obj, s):
        yield self.part('this')
        assert re.search(r'^[a-zA-Z$_][a-zA-Z$_0-9]*$', s), "Attribute name must match with regex: ^[a-zA-Z$_][a-zA-Z$_0-9]*$"
        yield self.part(obj, '.', s, name=True)
