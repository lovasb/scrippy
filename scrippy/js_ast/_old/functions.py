import json

from javascripthon.js_ast import JSDict
from javascripthon.js_ast.blocks import JSBlock
from javascripthon.transformations import _normalize_dict_keys


class JSFunction(JSBlock):
    @property
    def header_line(self):
        if self.is_method:
            line = ['static']
        else:
            line = ['function']

        line.append(' ')

        assert self.name is not None
        line.append(self.name)
        line += ['($args, $kwargs) {']
        return line

    @property
    def argparser_line(self):
        line = []
        definition = {'args': [], 'kwarg': []}
        for arg in self.python_args.args:
            line.append(arg.arg)
            definition['args'].append(arg.arg)

        if self.python_args.vararg:
            definition['vararg'] = self.python_args.vararg.arg

        if self.python_args.kwarg:
            line.append(self.python_args.kwarg.arg)
            definition['kwarg'] = self.python_args.kwarg.arg

        definition['kwonlyargs'] = self.python_args.kwonlyargs
        definition['kw_defaults'] = self.python_args.kw_defaults

        if hasattr(self.python_args, 'defaults'):
            definition['defaults'] = [_ for _ in self.python_args.defaults]

        line = ', '.join(line)

        # line = f'let [{line}] = $S.func_argparser($args, $kwargs, {json.dumps(definition)})'
        # definition.keys(), definition.values()
        line = ['let [', line, '] = ', '$S.func_argparser($args, $kwargs, ', self.part(*['{', 'valami: ', '7', '}'])]
        return line

    def emit(self, body, args):
        yield self.line(self.header_line)
        yield self.line(self.argparser_line, indent=True, delim=True)
        yield from self.lines(body, indent=True, delim=True)
        yield self.line('}')
