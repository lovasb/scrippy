# -*- coding: utf-8 -*-
# :Project:   metapensiero.pj -- classes
# :Created:   gio 08 feb 2018 02:32:04 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

from javascripthon.js_ast.blocks import JSBlock
from javascripthon.js_ast.functions import JSFunction


class JSClass(JSBlock):

    def emit(self, name, super_, methods):
        super_ = ','.join([f"'{_}'" for _ in super_])
        super_ = super_.replace("'object'", "'$S.object'")

        yield self.line([f"export let {name} = new $S.Type('{name}', [{super_}], class {{"])
        yield from self.lines(methods, indent=True, delim=True)
        yield self.line('}')


class JSClassMember(JSFunction):
    def with_kind(self, kind, args, body, acc=None, kwargs=None):
        line = ['static ', kind]
        line += self.fargs(args, acc, kwargs)
        line += ['{']
        yield self.line(line)
        yield from self.lines(body, indent=True, delim=True)
        yield self.line('}')


class JSClassConstructor(JSClassMember):
    def emit(self, args, body, acc=None, kwargs=None):
        yield from self.with_kind('constructor', args, body, acc, kwargs)


class JSMethod(JSClassMember):

    def emit(self, name, args, body, acc=None, kwargs=None, static=False):
        yield from self.with_kind(name, args, body, acc, kwargs, static)


class JSAsyncMethod(JSClassMember):

    def emit(self, name, args, body, acc=None, kwargs=None, static=False):
        yield from self.with_kind('async ' + name, args, body, acc, kwargs,
                                  static)


class JSGenMethod(JSClassMember):

    def emit(self, name, args, body, acc=None, kwargs=None, static=False):
        yield from self.with_kind('* ' + name, args, body, acc, kwargs,
                                  static)


class JSGetter(JSClassMember):

    def emit(self, name, body, static=False):
        yield from self.with_kind('get ' + name, [], body, static=static)


class JSSetter(JSClassMember):

    def emit(self, name, arg, body, static=False):
        yield from self.with_kind('set ' + name, [arg], body, static=static)
