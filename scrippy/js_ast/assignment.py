import ast

from .base import JSNode
from .naming import JSName


class JSAssign(JSNode, ast.Assign):
    def emit(self, generator: 'SourceGenerator'):
        line = [
            'let [',
        ]

        assert len(self.targets) == 1  # mi van itt a többszörös targettel, ugyanis ha több értéket adok vissza az is egy tuple??
        target = self.targets[0]
        if hasattr(target, 'elts'):
            iterable = target.elts
        else:
            iterable = [target]

        for i, t in enumerate(iterable, start=1):
            line.append(t.emit(generator))
            if i < len(iterable):
                line.append(', ')

        line.append('] = ')

        line.extend(*[self.value.emit(generator)])
        yield self.line(line)
