import ast
from .base import JSNode
from .naming import JSName

if False:
    from ..output.generator import SourceGenerator


class JSCall(JSNode, ast.Call):
    def emit(self, generator: 'SourceGenerator'):
        arr = [self.py_node.func.id, '(']
        if isinstance(self.func, JSName) and self.func.id == 'print':
            arr[0] = 'console.print'

        arr.append('[')
        for i, arg in enumerate(self.args, start=1):
            arr.append(arg.emit(generator))
            if i < len(self.args):
                arr.append(', ')
        arr.append('], {')

        for i, kw in enumerate(self.keywords, start=1):
            arr += [f'{kw.arg}: ', *kw.value.emit(generator)]
            if i < len(self.keywords):
                arr.append(', ')

        arr.append('})')

        yield self.part(*arr)
