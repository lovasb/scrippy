import ast
from .base import JSNode


class JSName(JSNode, ast.Name):
    def emit(self, generator: 'SourceGenerator'):
        # TODO: check name
        yield self.part(*[self.id])


class JSConstant(JSNode, ast.Constant):
    def emit(self, generator: 'SourceGenerator'):
        if isinstance(self.value, str):
            v = f'"{self.value}"'
        else:
            v = f'{self.value}'

        yield self.part(*[v])
