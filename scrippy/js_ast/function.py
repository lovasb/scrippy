import ast

from .base import JSNode


class JSFunctionDef(JSNode, ast.FunctionDef):
    @property
    def is_method(self):
        """if isinstance(t.parent_of(x), ast.ClassDef):  # it is a class method
            return True"""
        return False

    @property
    def header_line(self):
        if self.is_method:
            line = ['static']
        else:
            line = ['function']

        line.append(' ')

        assert self.name is not None
        line.append(self.name)
        line += ['($args, $kwargs) {']
        return line

    @property
    def argparser_line(self):
        line = []
        definition = {'args': [], 'kwarg': []}
        args = self.py_node.args
        for arg in args.args:
            line.append(arg.arg)
            definition['args'].append(arg.arg)

        if args.vararg:
            definition['vararg'] = args.vararg.arg

        if args.kwarg:
            line.append(args.kwarg.arg)
            definition['kwarg'] = args.kwarg.arg

        definition['kwonlyargs'] = args.kwonlyargs
        definition['kw_defaults'] = args.kw_defaults

        if hasattr(self.args, 'defaults'):
            definition['defaults'] = [_ for _ in args.defaults]

        line = [
            'let [',
            ', '.join(line),
            '] = ',
            '$S.func_argparser($args, $kwargs, {'
        ]

        line += ['"args": [',]
        line += ','.join(f'"{arg}"'for arg in definition['args'])
        line += f'], "kwarg": "{definition["kwarg"]}"'
        # line += ','.join(f'"{arg}"' for arg in )
        line += ', "kwonlyargs": ['
        line += ','.join(f'"{arg}"' for arg in definition['kwonlyargs'])
        line += '], "kw_defaults": ['
        line += ','.join(f'"{arg}"' for arg in definition['kw_defaults'])
        line += '], "defaults": ['
        line += ','.join(f'"{arg}"' for arg in definition['defaults'])
        line.append(']})')

        return line

    def emit(self, generator):
        yield self.line(self.header_line)
        yield self.line(self.argparser_line, indent=True, delim=True)

        yield from self.lines(self.body, indent=True, delim=True)

        yield self.line('}')
