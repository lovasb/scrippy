import ast

from .base import JSNode


class JSReturn(JSNode, ast.Return):
    def emit(self, generator: 'SourceGenerator'):
        if self.py_node.value:  # TODO: self.value nincs, mert nincs JSTuple
            retval = ['return']
            if isinstance(self.py_node.value, ast.Name):
                retval += [' ', self.py_node.value]
            elif isinstance(self.py_node.value, (ast.List, ast.Tuple)):
                retval.append(' [')
                for i, node in enumerate(self.py_node.value.elts, start=1):
                    retval += list(node.emit(generator))
                    if i < len(self.py_node.value.elts):
                        retval.append(', ')
                retval.append(']')
            else:
                raise NotImplementedError

        yield self.line(retval, delim=True)
