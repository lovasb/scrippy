from .api import Api

api = Api()

__all__ = ['api']
