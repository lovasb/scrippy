import ast

from .transform import Python2Javascript

class Api:
    def dump_ast(self, source_path):
        #tree = parser.parse_file(source_path)
        pass

    def transform_ast(self, tree: ast.AST):
        return Python2Javascript().generic_visit(tree)

    def transform_source(self, source_path):
        from javascripthon.__main__ import transform
        transform(source_path, enable_es6=True)

    def transpile_string(self, source):
        from javascripthon.__main__ import transform_string
        return transform_string(source, enable_es6=True)


api = Api()
