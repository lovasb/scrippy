import ast

from .. import js_ast


class Python2Javascript(ast.NodeTransformer):
    """Convert Python based AST to Javascript nodes"""
    def pynode2jsnode(self, node: ast.AST) -> js_ast.JSNode:
        klass = getattr(js_ast, f'JS{node.__class__.__name__}', None)
        if klass:
            return klass(node)

        return node

    """def visit_Expr(self, node):
        self.generic_visit(node)
        return self.pynode2jsnode(node)

    def visit_Call(self, node):
        self.generic_visit(node)
        return self.pynode2jsnode(node)

    def visit_Name(self, node):
        self.generic_visit(node)
        return self.pynode2jsnode(node)"""

    def generic_visit(self, node):
        super().generic_visit(node)
        return self.pynode2jsnode(node)
