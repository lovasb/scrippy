import ast
import inspect
import re


class OutputSrc:
    def __init__(self, node, name=None):
        self.node = node
        self.src_name = name

    def _gen_mapping(self, text, src_line=None, src_offset=None,
                     dst_offset=None):
        """Generate a single mapping. `dst_line` is absent from signature
        because the part hasn't this information, but is present in the
        returned mapping. `src_line` is adjusted to be 0-based.

        See `Source Map version 3 proposal
        <https://docs.google.com/document/d/1U1RGAehQwRypUTovF1KRlpiOFze0b-_2gc6fAH0KY0k>`_.
        """
        return {
            'src_line': src_line,
            'src_offset': src_offset,
            'dst_line': None,
            'dst_offset': dst_offset,
            'text': text,
            'name': self.src_name if self.src_name is not True else str(self),
            'part': self
        }

    def _pos_in_src(self):
        """Return the position in source of the generated node."""
        py_node = self.node.py_node
        if py_node:
            offset = getattr(py_node, 'col_offset', 0)
            # multi-line comments have an offset of -1
            if offset < 0:
                offset = 0

            # special handling of nodes that are decorable. Those nodes expose
            # a 'lineno' that starts with the first decorator. for now, take
            # the last decorator lineno and add one
            if isinstance(py_node, (ast.FunctionDef, ast.AsyncFunctionDef,
                                    ast.ClassDef)) and py_node.decorator_list:
                result = (py_node.decorator_list[-1].lineno + 1, offset)
            else:
                result = (getattr(py_node, 'lineno', None),
                          offset)
        else:
            result = (None, None)
        return result


class Line(OutputSrc):

    def __init__(self, node, item, indent=False, delim=False, name=None):
        super().__init__(node, name)
        self.indent = int(indent)
        self.delim = delim
        if isinstance(item, (tuple, list)):
            item = Part(node, *item)
        self.item = item

    def __str__(self):
        line = str(self.item)
        if self.delim:
            line += ';'
        if self.indent and line.strip():
            line = (' ' * 4 * self.indent) + line
        line += '\n'
        return line

    def serialize(self, generator):
        yield self

    def src_mappings(self):
        src_line, src_offset = self._pos_in_src()
        offset = self.indent * 4
        if isinstance(self.item, str):
            if src_line:
                yield self._gen_mapping(self.item, src_line, src_offset,
                                        offset)
        else:
            assert isinstance(self.item, Part)
            for m in self.item.src_mappings():
                m['dst_offset'] += offset
                yield m

    def __repr__(self):
        return '<%s indent: %d, "%s">' % (self.__class__.__name__,
                                          self.indent, str(self))


class Part(OutputSrc):
    def __init__(self, node, *items, name=None):
        super().__init__(node, name)
        self.items = []
        for i in items:
            if isinstance(i, (str, Part)):
                self.items.append(i)
            elif inspect.isgenerator(i):
                self.items.extend(i)
            else:
                self.items.append(str(i))

    def __str__(self):
        return ''.join(str(i) for i in self.items)

    def serialize(self, generator):
        yield self

    def src_mappings(self):
        # optional position in source file, if this is missing, there's no
        # reason for generate a source mapping. (not all python AST elements
        # can be source located)
        src_line, src_offset = self._pos_in_src()
        # accumulator for string text
        frag = ''
        col = 0
        # for every item that composes this part...
        for i in self.items:
            assert isinstance(i, (str, Part))
            if isinstance(i, str):
                # if it's a string, just add it to the accumulator (usually
                # comma, parens, etc...)
                frag += i
            elif isinstance(i, Part):
                # if the item is a part
                if frag and src_line:
                    # .. and if there is accumulated text and a src location
                    # emit a src mapping for the accumulated text and reset it
                    yield self._gen_mapping(frag, src_line, src_offset, col)
                col += len(frag)
                frag = ''
                psrc = str(i)
                # ... then , ask the subpart to produce a src mapping and
                # maybe relocate it if necessary
                # yield from i._translate_src_mappings(i, src, psrc, col)
                for m in i.src_mappings():
                    m['dst_offset'] += col
                    yield m
                col += len(psrc)
        else:
            # at the end of the loop, if there is still a fragment and a src
            # location, emit a mapping for it
            if frag and src_line:
                yield self._gen_mapping(frag, src_line, src_offset, col)

    def _translate_src_mappings(self, part, src=None, psrc=None, start=None):
        src = src or str(self)
        psrc = psrc or str(part)
        offset = src.find(psrc, start)
        for m in part.src_mappings():
            m['dst_offset'] += offset
            yield m

    def __repr__(self):
        return '<%s, "%s">' % (self.__class__.__name__,
                               str(self))


def linecounter(iterable, start=1):
    count = start
    for line in iterable:
        yield count, line
        count += len(re.findall('\n', str(line)))
