import ast

from . import sourcemaps
from .base import OutputSrc, linecounter


class JavascriptModule(OutputSrc):
    def __init__(self, module: ast.AST):
        super().__init__(None)

        from .generator import SourceGenerator
        self.gen = SourceGenerator()
        self.gen.visit(module)

    @property
    def lines(self):
        return self.gen.lines

    def src_mappings(self, src_offset=None, dst_offset=None):
        sline_offset, scol_offset = src_offset or (0, 0)
        dline_offset, dcol_offset = dst_offset or (0, 0)
        for ix, line in linecounter(self.lines, start=1):
            for m in line.src_mappings():
                m['dst_line'] = ix + dline_offset
                m['dst_offset'] += dcol_offset
                m['src_line'] += sline_offset
                m['src_offset'] += scol_offset
                yield m

    def read(self):
        return ''.join(str(l) for l in self.lines)

    def sourcemap(self, source, src_filename, src_offset=None,
                  dst_offset=None):
        Token = sourcemaps.Token
        tokens = []
        for m in self.src_mappings(src_offset, dst_offset):
            token = Token(m['dst_line'] - 1, m['dst_offset'], src_filename,
                          m['src_line'] - 1, m['src_offset'], m['name'], m)
            tokens.append(token)

        src_map = sourcemaps.SourceMap(
            sources_content={src_filename: source}
        )
        for t in tokens:
            src_map.add_token(t)
        return src_map
