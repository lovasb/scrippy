import ast

from .. import js_ast


class SourceGenerator(ast.NodeVisitor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lines = []
        self.indent = 0

    def visit_JSExpr(self, node):
        for line in node.emit(generator=self):
            self.lines.append(line)

    def visit_JSCall(self, node: js_ast.JSCall):
        # self.lines += node.serialize(generator=self)
        self.generic_visit(node)

    def visit_JSFunctionDef(self, node):
        for line in node.emit(generator=self):
            self.lines.append(line)

    def visit_JSAssign(self, node):
        for line in node.emit(generator=self):
            self.lines.append(line)

        self.generic_visit(node)

    def visit_JSReturn(self, node):
        for line in node.serialize(generator=self):
            self.lines.append(line)

        self.generic_visit(node)

    def visit_Module(self, node: ast.Module):
        self.generic_visit(node)
