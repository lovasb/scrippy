#!/env/bin/python
import argparse
from pathlib import Path

from scrippy import api

parser = argparse.ArgumentParser(
    description="A Python 3.5+ to ES6 JavaScript compiler",
    prog='py2js'
)
parser.add_argument('file', type=str,
                    help="Python source file to convert")
parser.add_argument('--dump_ast', action='store_true', help="Dump Python source AST")

if __name__ == '__main__':
    args = parser.parse_args()

    source_path = Path(args.file)

    if args.dump_ast:
        api.dump_ast(source_path)
    else:
        api.transform_source(source_path)
